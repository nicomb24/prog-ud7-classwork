package com.mengual.prog.A2;

public class Coche {

    private String matricula;
    private String color;
    private String modelo;
    private int cilindrada;
    private int numeroPuertas;
    private String fechaMatriculacion;

    public Coche(String matricula, String color, String modelo, int cilindrada, int numeroPuertas) {
        this.matricula = matricula;
        this.color = color;
        this.modelo = modelo;
        this.cilindrada = cilindrada;
        this.numeroPuertas = numeroPuertas;
    }

    public String getMatricula(){

        return this.matricula;

    }

    @Override
    public String toString() {

        return "Matrícula: " + matricula + ", " +
                "Color: " + color + ", " +
                "Modelo: " + modelo + ", " +
                "Cilindrada: " + cilindrada + ", " +
                "Número de puertas: " + numeroPuertas + "\n";

    }
}
