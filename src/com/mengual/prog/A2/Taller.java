package com.mengual.prog.A2;

import java.util.ArrayList;

public class Taller {

    private ArrayList<Coche> llistatEnReparacio;
    private ArrayList<Coche> llistatReparats;

    public Taller() {

        this.llistatEnReparacio = new ArrayList<>();
        this.llistatReparats = new ArrayList<>();

    }

    public void registrarCoche(Coche coche){

        this.llistatEnReparacio.add(coche);

    }

    private Coche getCocheReparado(String matricula){

        for (int i = 0; i < llistatReparats.size(); i++ ){

            Coche coche = llistatReparats.get(i);

            if (coche.getMatricula().equalsIgnoreCase(matricula)){
                return coche;
            }

        }

        return null;

    }

    private Coche getCocheEnRaparacion(String matricula){

        for (int i = 0; i < llistatEnReparacio.size(); i++ ){

            Coche coche = llistatEnReparacio.get(i);

            if (coche.getMatricula().equalsIgnoreCase(matricula)){
                return coche;
            }

        }

        return null;

    }

    public void registrarReparado(String matricula){

        Coche coche = this.getCocheEnRaparacion(matricula);

        if (coche != null){

            this.llistatEnReparacio.remove(coche);
            this.llistatReparats.add(coche);

        }

    }

    public void registrarSalida(String matricula){

        Coche coche = this.getCocheReparado(matricula);

        if (coche != null){

            this.llistatReparats.remove(coche);

        }

    }

    public void listarEnReparacion(){

        for (int i = 0; i < llistatEnReparacio.size(); i++){

            Coche coche = this.llistatEnReparacio.get(i);

            System.out.println(coche);

        }

    }

    public void listarReparados(){

        for (int i = 0; i < llistatReparats.size(); i++){

            Coche coche = this.llistatReparats.get(i);

            System.out.println(coche);

        }

    }
}
