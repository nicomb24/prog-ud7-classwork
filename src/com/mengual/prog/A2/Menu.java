package com.mengual.prog.A2;

import java.util.Scanner;

public class Menu {

    private Taller taller;

    private Scanner scanner;

    private final int OP_REGISTRAR = 1;
    private final int OP_LISTA_EN_REPARACION = 2;
    private final int OP_LISTA_REPARADOS = 3;
    private final int OP_REGISTRAR_REPARADO = 4;
    private final int OP_REGISTRAR_SALIDA = 5;
    private final int OP_EXIT = 6;

    public Menu() {

        this.taller = new Taller();
        this.scanner = new Scanner(System.in);

    }

    private int eleccionMenu(){

        System.out.println(OP_REGISTRAR + ". Registrar coche");
        System.out.println(OP_LISTA_EN_REPARACION + ". Listado de coches en reparación");
        System.out.println(OP_LISTA_REPARADOS + ".    Listado de coches reparados");
        System.out.println(OP_REGISTRAR_REPARADO + ". Registrar coche reparado");
        System.out.println(OP_REGISTRAR_SALIDA + ". Registrar salida de coche");
        System.out.println(OP_EXIT + ". Salir");

        return scanner.nextInt();

    }

    public void menu(){

        int opcion;

        do {

            opcion = eleccionMenu();

            if (opcion == OP_REGISTRAR) {
                registrarCoche();
            } else if (opcion == OP_LISTA_EN_REPARACION) {
                listarEnReparacion();
            } else if (opcion == OP_LISTA_REPARADOS) {
                listarReparados();
            } else if (opcion == OP_REGISTRAR_REPARADO) {
                registrarReparado();
            } else if (opcion == OP_REGISTRAR_SALIDA) {
                registrarSalida();
            }

        } while (opcion != OP_EXIT);

    }

    private void registrarCoche(){

        System.out.println("Introduce la matrícula");
        String matricula = scanner.next();

        System.out.println("Introduce el color");
        String color = scanner.next();

        System.out.println("Introduce el modelo");
        String modelo = scanner.next();

        System.out.println("Introduce la cilindrada");
        int cilindrada = scanner.nextInt();

        System.out.println("Introduce las puertas");
        int puertas = scanner.nextInt();

        Coche cocheNuevo = new Coche(matricula, color, modelo, cilindrada, puertas);
        taller.registrarCoche(cocheNuevo);

    }

    private void registrarReparado(){

        taller.listarEnReparacion();

        System.out.println("Introduce la matrícula del coche que quiere marcar como reparado");

        taller.registrarReparado(scanner.next());

    }

    private void registrarSalida(){

        taller.listarReparados();

        System.out.println("Introduce la matrícula del coche que quiere registrar como salida");

        taller.registrarSalida(scanner.next());

    }

    private void listarEnReparacion(){

        taller.listarEnReparacion();

    }

    private void listarReparados(){

        taller.listarReparados();

    }


}
