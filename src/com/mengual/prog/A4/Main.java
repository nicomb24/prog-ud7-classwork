package com.mengual.prog.A4;

public class Main {

    public static LocalStack data;

    public static void main(String [] args){

        showReverted("Ejemplo uso pila");

    }

    public static void showReverted(String cadena){

        data = new LocalStack();

        System.out.println("Normal: " + cadena);

        for (int i = 0; i < cadena.length(); i++){

            data.push(cadena.charAt(i));

        }

        System.out.print("Reverse: ");

        while (!data.isEmpty()){

            System.out.print(data.pop());

        }

    }

}
