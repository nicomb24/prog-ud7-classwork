package com.mengual.prog.A4;

import java.util.ArrayList;

public class LocalStack {

    private ArrayList<Object> data;

    public LocalStack() {

        this.data = new ArrayList<>();

    }

    public void push(Object object){

        data.add(object);

    }

    public Object pop(){

        return data.remove(data.size()-1);

    }

    public boolean isEmpty(){

        return (data.size() <= 0);

    }
}
