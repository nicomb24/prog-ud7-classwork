package com.mengual.prog.A3;

import java.util.ArrayList;

public class Comanda {

    private int codi;

    private ArrayList<Producte> productes;

    private String fecha;

    private String nomClient;

    public Comanda(int codi, String fecha, String nomClient) {

        this.codi = codi;
        this.fecha = fecha;
        this.nomClient = nomClient;
        this.productes = new ArrayList<>();

    }

    public void addProducte(Producte producte){

        productes.add(producte);

    }

    public Producte getProducteComanda(int codi){

        for (int i = 0; i < productes.size(); i++){

            Producte producte = productes.get(i);

            if (producte.getCodi() == codi){

                return producte;

            }

        }

        return null;

    }

    private void showProductes(){

        for (int i = 0; i < productes.size(); i++){

            System.out.println(productes.get(i));

        }

    }

    private float calcularPreu(){

        float preu = 0;

        for (int i = 0; i < productes.size(); i++){

            preu += productes.get(i).getPvp();

        }

        return preu;

    }

    @Override
    public String toString() {
        return codi + ":\n" +
                "Preu: " + calcularPreu() + "€";
    }
}
