package com.mengual.prog.A3;

import java.util.ArrayList;

public class CatalogoProductos {

    private ArrayList<Producte> catalogoProductos;

    public CatalogoProductos() {

        this.catalogoProductos = new ArrayList<>();

    }

    public void addProducto(Producte producte){

        catalogoProductos.add(producte);

    }

    public void  removeProducto(Producte producte){

        catalogoProductos.remove(producte);

    }

    public Producte getProducteCatalogo(int codi){

        for (int i = 0; i < catalogoProductos.size(); i++){

            Producte producte = catalogoProductos.get(i);

            if (producte.getCodi() == codi){

                return producte;

            }

        }

        return null;

    }

    public void showCatalogo(){

        for (int i = 0; i < catalogoProductos.size(); i++){

            System.out.print(catalogoProductos.get(i));

        }

    }

}
