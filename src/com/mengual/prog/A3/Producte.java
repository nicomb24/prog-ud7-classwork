package com.mengual.prog.A3;

public class Producte {

    private int codi;

    private String nom;

    private float preu;

    private int descompte;

    private int iva;

    public Producte(int codi, String nom, float preu, int descompte, int iva) {
        this.codi = codi;
        this.nom = nom;
        this.preu = preu;
        this.descompte = descompte;
        this.iva = iva;
    }

    public void setNom(String nom) {

        this.nom = nom;

    }

    public void setCodi(int codi) {

        this.codi = codi;

    }

    public void setDescompte(int descompte) {

        this.descompte = descompte;

    }

    public void setIva(int iva) {

        this.iva = iva;

    }

    public void setPreu(float preu) {

        this.preu = preu;

    }

    public int getCodi() {

        return codi;

    }

    public float getPvp(){

        return  ((preu * iva) / 100) - ((preu * descompte) / 100);

     }

    @Override
    public String toString() {
        return  codi +
                ". Nom: " + nom +
                "---> " + preu + "€\n\n";
    }
}
