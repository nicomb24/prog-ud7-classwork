package com.mengual.prog.A3;

import sun.tools.tree.NewArrayExpression;

import java.util.Scanner;

public class Empresa {

    private Scanner scanner;

    private CatalogoProductos catalogoProductos;

    private LlistaComandes llistaComandes;

    public Empresa() {

        this.scanner = new Scanner(System.in);
        this.catalogoProductos = new CatalogoProductos();
        this.llistaComandes = new LlistaComandes();

    }

    public void newProducto(){

        System.out.println("Introduce el código del producto");
        int codigo = scanner.nextInt();

        System.out.println("Introduce el nombre del producto");
        String nombre = scanner.next();

        System.out.println("Introduce el precio del producto");
        float precio = scanner.nextFloat();

        System.out.println("Introduce el descuento del producto");
        int descuento = scanner.nextInt();

        System.out.println("Introduce el IVA del producto");
        int iva = scanner.nextInt();

        Producte newProducte = new Producte(codigo, nombre, precio, descuento, iva);

        catalogoProductos.addProducto(newProducte);

    }

    public void removeProductoEmpresa(){

        catalogoProductos.showCatalogo();

        System.out.println("Introduce el código del producto que deseas eliminar");
        int codigo = scanner.nextInt();

        Producte producte = catalogoProductos.getProducteCatalogo(codigo);

        catalogoProductos.removeProducto(producte);

    }

    public void modProducto(){

        catalogoProductos.showCatalogo();

        System.out.println("Introduce el código del producto que deseas modificar");
        int codigo = scanner.nextInt();

        Producte producte = catalogoProductos.getProducteCatalogo(codigo);

    }

    public void showCatalogo(){

        catalogoProductos.showCatalogo();

    }

    public void newComanda(){

        System.out.println("Introduce el código de la comanda");
        int codigo = scanner.nextInt();

        System.out.println("Introduce el nombre del cliente");
        String cliente = scanner.next();

        System.out.println("Introduce la fecha (dd/mm/yyyy)");
        String fecha = scanner.next();

        Comanda comanda = new Comanda(codigo, fecha, cliente);

        int opcion;

        catalogoProductos.showCatalogo();

        do {

            System.out.println("Introduce el código del producto que deseas añadir (0 para salir)");
            opcion = scanner.nextInt();

            Producte producte = catalogoProductos.getProducteCatalogo(opcion);

            comanda.addProducte(producte);

        } while (opcion != 0);

        llistaComandes.addComanda(comanda);

    }

    public void showComandes(){

        llistaComandes.showComandes();

    }
}
