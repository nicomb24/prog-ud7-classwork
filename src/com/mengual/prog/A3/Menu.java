package com.mengual.prog.A3;

import java.util.Scanner;

public class Menu {

    private Empresa empresa;

    private Scanner scanner;

    private final int OP_ANADIR_PRODUCTO = 1;
    private final int OP_VER_CATALOGO = 2;
    private final int OP_CREAR_COMANDA = 3;
    private final int OP_VER_COMANDA = 4;
    private final int OP_EXIT = 10;

    public Menu() {

        this.empresa = new Empresa();
        this.scanner = new Scanner(System.in);

    }

    private int eleccionMenu(){

        System.out.println(OP_ANADIR_PRODUCTO + ". Añadir producto al catálogo");
        System.out.println(OP_VER_CATALOGO + ". Listado de productos");
        System.out.println(OP_CREAR_COMANDA + ". Crear un pedido nuevo");
        System.out.println(OP_VER_COMANDA + ". Ver pedido");
        System.out.println(OP_EXIT + ". Salir");

        return scanner.nextInt();

    }

    public void menu(){

        int opcion;

        do {

            opcion = eleccionMenu();

            if (opcion == OP_ANADIR_PRODUCTO) {
                empresa.newProducto();
            } else if (opcion == OP_VER_CATALOGO) {
                empresa.showCatalogo();
            } else if (opcion == OP_CREAR_COMANDA) {
                empresa.newComanda();
            } else if (opcion == OP_VER_COMANDA) {
                empresa.showComandes();
            }

        } while (opcion != OP_EXIT);

    }

}
