package com.mengual.prog.A3;

import java.util.ArrayList;

public class LlistaComandes {

    private ArrayList<Comanda> llistaComandes;

    public LlistaComandes() {

        this.llistaComandes = new ArrayList<>();

    }

    public void addComanda(Comanda comanda){

        llistaComandes.add(comanda);

    }

    public void showComandes(){

        for (int i = 0; i < llistaComandes.size(); i++) {

            System.out.print(llistaComandes.get(i));

        }
    }

}
