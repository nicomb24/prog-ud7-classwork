package com.mengual.prog.A1;

public class Empleat {

    private String nom;

    private float sou;

    public Empleat(String nom, float sou) {
        this.nom = nom;
        this.sou = sou;
    }

    @Override
    public String toString() {
        return "Nom: " + nom + "\n"
                + "Sou: " + sou;
    }
}
