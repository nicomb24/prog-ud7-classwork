package com.mengual.prog.A1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);
        Empresa empresa = new Empresa();

        do {

            System.out.println("Vols insertar les dades d'algun empleat? ");

            if (!scanner.next().equalsIgnoreCase("S")){

                break;

            } else {

                empresa.addEmpleat();

            }

        } while (true);

        empresa.showEmpleats();

    }

}
