package com.mengual.prog.A1;

import java.util.ArrayList;
import java.util.Scanner;

public class Empresa {

    private Scanner scanner;

    private ArrayList<Empleat> empleats;

    public Empresa() {

        this.scanner = new Scanner(System.in);
        this.empleats = new ArrayList<>();

    }

    public void addEmpleat(){

        System.out.println("Introduce el nombre: ");
        String nom = scanner.nextLine();

        System.out.println("Introduce el sueldo: ");
        float sou = scanner.nextFloat();

        Empleat empleat = new Empleat(nom, sou);

        this.empleats.add(empleat);

    }

    public void showEmpleats(){

        for (Empleat empleat: empleats){

            System.out.print(empleat);

        }

    }

}
