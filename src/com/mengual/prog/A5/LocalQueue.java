package com.mengual.prog.A5;

import java.util.ArrayList;

public class LocalQueue {

    private ArrayList<Object> data;

    public LocalQueue() {

        this.data = new ArrayList<>();

    }

    public void push(Object object){

        data.add(object);

    }

    public Object pop(){

        return data.remove(0);

    }

    public boolean isEmpty(){

        return (data.size() <= 0);

    }
}
