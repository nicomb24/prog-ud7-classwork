package com.mengual.prog.A5;

public class Main {

    public static LocalQueue data;

    public static void main(String[] args){

        data = new LocalQueue();

        addMessage();
        consumMessage();

    }

    public static void addMessage(){

        for (int i = 0; i < 10; i++){

            data.push("message" + i);

        }

    }

    public static void consumMessage(){

        while (!data.isEmpty()){

            System.out.println(data.pop());

        }

    }

}
